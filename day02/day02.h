// day02.h : main header file for the DAY02 application
//

#if !defined(AFX_DAY02_H__2F099763_61D8_42AC_A663_661570A818BE__INCLUDED_)
#define AFX_DAY02_H__2F099763_61D8_42AC_A663_661570A818BE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDay02App:
// See day02.cpp for the implementation of this class
//

class CDay02App : public CWinApp
{
public:
	CDay02App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDay02App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDay02App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DAY02_H__2F099763_61D8_42AC_A663_661570A818BE__INCLUDED_)
