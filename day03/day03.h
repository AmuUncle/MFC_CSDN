// day03.h : main header file for the DAY03 application
//

#if !defined(AFX_DAY03_H__C27F732B_A19E_4B58_A390_0C365EFDEC97__INCLUDED_)
#define AFX_DAY03_H__C27F732B_A19E_4B58_A390_0C365EFDEC97__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDay03App:
// See day03.cpp for the implementation of this class
//

class CDay03App : public CWinApp
{
public:
	CDay03App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDay03App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDay03App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DAY03_H__C27F732B_A19E_4B58_A390_0C365EFDEC97__INCLUDED_)
