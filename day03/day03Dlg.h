// day03Dlg.h : header file
//

#if !defined(AFX_DAY03DLG_H__6FCB3A7E_DCE4_409D_B0BC_A9D9648E5B18__INCLUDED_)
#define AFX_DAY03DLG_H__6FCB3A7E_DCE4_409D_B0BC_A9D9648E5B18__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CDay03Dlg dialog

class CDay03Dlg : public CDialog
{
// Construction
public:
	CDay03Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDay03Dlg)
	enum { IDD = IDD_DAY03_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDay03Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDay03Dlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnButtonOk();
	afx_msg void OnDoubleclickedButtonCacel();
	afx_msg void OnButtonCacel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DAY03DLG_H__6FCB3A7E_DCE4_409D_B0BC_A9D9648E5B18__INCLUDED_)
