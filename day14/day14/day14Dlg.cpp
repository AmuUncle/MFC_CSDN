
// day14Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "day14.h"
#include "day14Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Cday14Dlg 对话框




Cday14Dlg::Cday14Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cday14Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cday14Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE_VIEW, m_tree);
}

BEGIN_MESSAGE_MAP(Cday14Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_ADD, &Cday14Dlg::OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, &Cday14Dlg::OnBnClickedButtonDel)
	ON_BN_CLICKED(IDC_BUTTON_MDY, &Cday14Dlg::OnBnClickedButtonMdy)
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE_VIEW, &Cday14Dlg::OnTvnSelchangedTreeView)
END_MESSAGE_MAP()


// Cday14Dlg 消息处理程序

BOOL Cday14Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void Cday14Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void Cday14Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR Cday14Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void Cday14Dlg::OnBnClickedButtonAdd()
{
	// TODO: 在此添加控件通知处理程序代码
	CString strText;
	GetDlgItemText(IDC_EDIT_ADD_MDY,strText);
	if (strText.GetLength() == 0)
	{
		AfxMessageBox(_T("请输入节点名！"));
		return;
	}
	HTREEITEM hItem = m_tree.GetSelectedItem();
	if(hItem == NULL){
		hItem = TVI_ROOT;
	}

	TVINSERTSTRUCT ts ={0};
	ts.hParent = hItem;
	ts.hInsertAfter = TVI_LAST;
	ts.item.pszText = strText.GetBuffer();
	ts.item.mask = TVIF_TEXT;
	HTREEITEM hNewItem = m_tree.InsertItem(&ts);
	m_tree.SelectItem(hNewItem);
	m_tree.EnsureVisible(hNewItem);
}


void Cday14Dlg::OnBnClickedButtonDel()
{
	// TODO: 在此添加控件通知处理程序代码
	HTREEITEM hItem = m_tree.GetSelectedItem();
	if(hItem == NULL){
		AfxMessageBox(_T("请选择要删除的节点！"));
		return;
	}

	HTREEITEM hParentItem = m_tree.GetParentItem(hItem);
	m_tree.DeleteItem(hItem);
	m_tree.SelectItem(hParentItem);
}


void Cday14Dlg::OnBnClickedButtonMdy()
{
	// TODO: 在此添加控件通知处理程序代码
	HTREEITEM hItem = m_tree.GetSelectedItem();
	if(hItem == NULL){
		AfxMessageBox(_T("请选择要修改的节点！"));
		return;
	}

	CString strText;
	GetDlgItemText(IDC_EDIT_ADD_MDY,strText);
	if (strText.GetLength() == 0)
	{
		AfxMessageBox(_T("请输入新的节点名！"));
		return;
	}
	m_tree.SetItemText(hItem,strText);

}


void Cday14Dlg::OnTvnSelchangedTreeView(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码

	HTREEITEM hItem = m_tree.GetSelectedItem();
	if(hItem != NULL){
		CString strText = m_tree.GetItemText(hItem);
		SetDlgItemText(IDC_EDIT_ADD_MDY, strText);
	}
	*pResult = 0;
}
