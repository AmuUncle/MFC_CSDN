
// day08Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "day08.h"
#include "day08Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Cday08Dlg 对话框




Cday08Dlg::Cday08Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cday08Dlg::IDD, pParent)
	, m_Height(0)
	, m_Weight(0)
	, m_BMI(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cday08Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_SG, m_Height);
	DDX_Text(pDX, IDC_EDIT_TZ, m_Weight);
	DDX_Text(pDX, IDC_EDIT_BMI, m_BMI);
	DDX_Text(pDX, IDC_EDIT_SG, m_Height);
	DDX_Text(pDX, IDC_EDIT_TZ, m_Weight);
	DDX_Text(pDX, IDC_EDIT_BMI, m_BMI);
}

BEGIN_MESSAGE_MAP(Cday08Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &Cday08Dlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &Cday08Dlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_FUNC4, &Cday08Dlg::OnBnClickedButtonFunc4)
	ON_BN_CLICKED(IDC_BUTTON_FUNC5, &Cday08Dlg::OnBnClickedButtonFunc5)
	ON_BN_CLICKED(IDC_BUTTON_FUNC6, &Cday08Dlg::OnBnClickedButtonFunc6)
	ON_BN_CLICKED(IDC_BUTTON_FUNC7, &Cday08Dlg::OnBnClickedButtonFunc7)
END_MESSAGE_MAP()


// Cday08Dlg 消息处理程序

BOOL Cday08Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void Cday08Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void Cday08Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR Cday08Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void Cday08Dlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码

	/* 第一种
	int nHeight= GetDlgItemInt(IDC_EDIT_SG,NULL,TRUE);
	double dHeight = nHeight / 100.00;
	int nWeight = GetDlgItemInt(IDC_EDIT_TZ,NULL,TRUE);
	double bmi = nWeight/(dHeight*dHeight);
	char buf[28];
	sprintf(buf, "%.3f", bmi);
	SetDlgItemText(IDC_EDIT_BMI,buf);
	*/

	/* 第二种
	TCHAR tcHeight[10], tcWeight[10] , tcBMI[10];
	GetDlgItem(IDC_EDIT_SG)->GetWindowText(tcHeight,10);
	GetDlgItem(IDC_EDIT_TZ)->GetWindowText(tcWeight,10);

	int nHeight = _ttoi(tcHeight);
	double dHeight = nHeight / 100.00;
	int nWeight = _ttoi(tcWeight);
	
	double bmi = nWeight/(dHeight * dHeight);
	char buf[28];
	sprintf(buf, "%.3f", bmi);
	GetDlgItem(IDC_EDIT_BMI)->SetWindowText(buf);
	*/

	TCHAR tcHeight[10], tcWeight[10] , tcBMI[10];
	GetDlgItemText(IDC_EDIT_SG,tcHeight,10);
	GetDlgItemText(IDC_EDIT_TZ,tcWeight,10);

	int nHeight = _ttoi(tcHeight);
	double dHeight = nHeight / 100.00;
	int nWeight = _ttoi(tcWeight);
	
	double bmi = nWeight/(dHeight * dHeight);
	char buf[28];
	sprintf(buf, "%.3f", bmi);
	SetDlgItemText(IDC_EDIT_BMI,buf);


}


void Cday08Dlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}


void Cday08Dlg::OnBnClickedButtonFunc4()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);   //把控件的值关联到变量
	double dHeight = m_Height / 100.00;
	m_BMI = m_Weight / (dHeight * dHeight );
	UpdateData(FALSE);  //把变量的值关联到控件
}


void Cday08Dlg::OnBnClickedButtonFunc5()
{
	// TODO: 在此添加控件通知处理程序代码
	// TODO: 在此添加控件通知处理程序代码
	TCHAR tcHeight[10], tcWeight[10] , tcBMI[10];
	m_edit_sg.GetWindowText(tcHeight,10);
	m_edit_tz.GetWindowText(tcWeight,10);

	int nHeight = _ttoi(tcHeight);
	double dHeight = nHeight / 100.00;
	int nWeight = _ttoi(tcWeight);

	double bmi = nWeight/(dHeight * dHeight);
	char buf[28];
	sprintf(buf, "%.3f", bmi);
	m_edit_bmi.SetWindowText(buf);
}


void Cday08Dlg::OnBnClickedButtonFunc6()
{
	// TODO: 在此添加控件通知处理程序代码
	TCHAR tcHeight[10], tcWeight[10] , tcBMI[10];
	::SendMessage(GetDlgItem(IDC_EDIT_SG)->m_hWnd,WM_GETTEXT,10,(LPARAM)tcHeight);
	::SendMessage(GetDlgItem(IDC_EDIT_TZ)->m_hWnd,WM_GETTEXT,10,(LPARAM)tcWeight);

	int nHeight = _ttoi(tcHeight);
	double dHeight = nHeight / 100.00;
	int nWeight = _ttoi(tcWeight);

	double bmi = nWeight/(dHeight * dHeight);
	char buf[28];
	sprintf(buf, "%.3f", bmi);
	::SendMessage(GetDlgItem(IDC_EDIT_BMI)->m_hWnd,WM_SETTEXT,10,(LPARAM)buf);
}


void Cday08Dlg::OnBnClickedButtonFunc7()
{
	// TODO: 在此添加控件通知处理程序代码
	TCHAR tcHeight[10], tcWeight[10] , tcBMI[10];
	SendDlgItemMessage(IDC_EDIT_SG,WM_GETTEXT,10,(LPARAM)tcHeight);
	SendDlgItemMessage(IDC_EDIT_TZ,WM_GETTEXT,10,(LPARAM)tcWeight);

	int nHeight = _ttoi(tcHeight);
	double dHeight = nHeight / 100.00;
	int nWeight = _ttoi(tcWeight);

	double bmi = nWeight/(dHeight * dHeight);
	char buf[28];
	sprintf(buf, "%.3f", bmi);

	SendDlgItemMessage(IDC_EDIT_BMI,WM_SETTEXT,10,(LPARAM)buf);
}
