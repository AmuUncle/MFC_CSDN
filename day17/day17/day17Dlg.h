
// day17Dlg.h : 头文件
//

#pragma once
#include "afxwin.h"


// Cday17Dlg 对话框
class Cday17Dlg : public CDialogEx
{
// 构造
public:
	Cday17Dlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_DAY17_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CButton m_chk_red;
	CButton m_chk_green;
	CButton m_chk_blue;
	CButton m_rd_squ;
	CButton m_rd_circ;
	afx_msg void OnBnClickedCheckBlue();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBnClickedCheckGreen();
	afx_msg void OnBnClickedCheckRed();
	afx_msg void OnBnClickedRadioSqurae();
	afx_msg void OnBnClickedRadioCirc();
};
