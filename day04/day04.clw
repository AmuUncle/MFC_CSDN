; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CChat
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "day04.h"

ClassCount=4
Class1=CDay04App
Class2=CDay04Dlg

ResourceCount=4
Resource2=IDD_DIALOG_ABOUT
Resource1=IDR_MAINFRAME
Class3=CAboutDlg
Resource3=IDD_DAY04_DIALOG
Class4=CChat
Resource4=IDD_DIALOG_CHAT

[CLS:CDay04App]
Type=0
HeaderFile=day04.h
ImplementationFile=day04.cpp
Filter=N

[CLS:CDay04Dlg]
Type=0
HeaderFile=day04Dlg.h
ImplementationFile=day04Dlg.cpp
Filter=D
LastObject=CDay04Dlg
BaseClass=CDialog
VirtualFilter=dWC



[DLG:IDD_DAY04_DIALOG]
Type=1
Class=CDay04Dlg
ControlCount=2
Control1=IDC_BUTTON_ABOUT,button,1342242816
Control2=IDC_BUTTON_CHAT,button,1342242816

[DLG:IDD_DIALOG_ABOUT]
Type=1
Class=CAboutDlg
ControlCount=1
Control1=IDC_STATIC,static,1342308352

[CLS:CAboutDlg]
Type=0
HeaderFile=AboutDlg.h
ImplementationFile=AboutDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CAboutDlg

[DLG:IDD_DIALOG_CHAT]
Type=1
Class=CChat
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,button,1342177287
Control4=IDC_EDIT1,edit,1350631552

[CLS:CChat]
Type=0
HeaderFile=Chat.h
ImplementationFile=Chat.cpp
BaseClass=CDialog
Filter=D
LastObject=CChat

