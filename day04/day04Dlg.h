// day04Dlg.h : header file
//
#include "AboutDlg.h"
#include "Chat.h"

#if !defined(AFX_DAY04DLG_H__5F7BE15A_4565_44CE_B9DF_4C2CA08B71FA__INCLUDED_)
#define AFX_DAY04DLG_H__5F7BE15A_4565_44CE_B9DF_4C2CA08B71FA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CDay04Dlg dialog

class CDay04Dlg : public CDialog
{
// Construction
public:
	CDay04Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDay04Dlg)
	enum { IDD = IDD_DAY04_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDay04Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDay04Dlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonAbout();
	afx_msg void OnButtonChat();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DAY04DLG_H__5F7BE15A_4565_44CE_B9DF_4C2CA08B71FA__INCLUDED_)
