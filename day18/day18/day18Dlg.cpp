
// day18Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "day18.h"
#include "day18Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Cday18Dlg 对话框




Cday18Dlg::Cday18Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cday18Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cday18Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Cday18Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_BTN_SETTEXT, &Cday18Dlg::OnBnClickedBtnSettext)
	ON_BN_CLICKED(IDC_BTN_SETTEXT2, &Cday18Dlg::OnBnClickedBtnSettext2)
END_MESSAGE_MAP()


// Cday18Dlg 消息处理程序

BOOL Cday18Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	m_hCursor = LoadCursor(NULL, IDC_SIZEALL);
	GetDlgItem(IDC_PIC)->GetWindowRect(&m_rtCtrl);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void Cday18Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void Cday18Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR Cday18Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void Cday18Dlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (point.x >= m_rtCtrl.left 
		&& point.x <= m_rtCtrl.right 
		|| point.y >= m_rtCtrl.top 
		&& point.y <= m_rtCtrl.bottom)
	{
		m_bCapturing = TRUE;
		SetCapture(); // 开始捕获鼠标
		SetCursor(m_hCursor);



	}

	CDialogEx::OnLButtonDown(nFlags, point);
}


void Cday18Dlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (m_bCapturing)
	{
		ReleaseCapture();
		m_bCapturing = FALSE;
		POINT pt = point;
		ClientToScreen(&pt);
		m_hWndDest = ::WindowFromPoint(pt);
		TCHAR szBuf[MAX_PATH] = {0};
		_stprintf(szBuf,_T("0x%.8X"),m_hWndDest);
		SetDlgItemText(IDC_EDIT_DESTHWND,szBuf);
		GetClassName(m_hWndDest,szBuf,MAX_PATH);
		SetDlgItemText(IDC_EDIT_DESTCLASS,szBuf);
		::PostMessage(m_hWndDest,WM_GETTEXT,MAX_PATH,(LPARAM)szBuf);
		SetDlgItemText(IDC_EDIT_DESTTEXT,szBuf);
	}

	CDialogEx::OnLButtonUp(nFlags, point);
}


void Cday18Dlg::OnBnClickedBtnSettext()
{
	// TODO: 在此添加控件通知处理程序代码
	CString strText;
	GetDlgItemText(IDC_EDIT_DESTTEXT,strText);
	::PostMessage(m_hWndDest,WM_SETTEXT,0,(LPARAM)(LPCTSTR)strText);
}


void Cday18Dlg::OnBnClickedBtnSettext2()
{
	// TODO: 在此添加控件通知处理程序代码
	//::SendMessage(m_hWndDest,WM_CLOSE,0,0);
	::PostMessage(m_hWndDest,WM_CLOSE,0,0);
}
